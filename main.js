new Vue({
    el: '#tasks',
    data: {
        tasks: [
            { body: '<script>alert("hi")</script>', completed: false }
        ],
        newTask: ''
    },
    computed: {
        completions: function() {
            return this.tasks.filter(function(task) {
                return task.completed;
            });
        }
    },
    filters: {
        inProcess: function(tasks) {
            return tasks.filter(function(task) {
                return !task.completed;
            })
        }
    },
    methods: {
        addTask: function() {
            this.tasks.push({
                body: this.newTask,
                completed: false
            });
            this.newTask = '';
            this.$els.newTask.focus();
        },
        removeTask: function(task) {
            this.tasks.$remove(task);
        },
        editTask: function(task) {
            this.removeTask(task);
            this.newTask = task.body;
            console.log(this.$els);
            this.$els.newTask.focus();
        },
        completeTask: function(task) {
            task.completed = true;
        }
    }
})